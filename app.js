const {createWaBot} = require('./wa/wa.bot')
const {TIME_LIMIT_RUNNING, dateFormat, getOneHourLater} = require('./utils/utils')
const express = require('express');
const app = express();
const port = 8080;


createWaBot()

app.listen(port, () => {
    const dateRun = new Date()
    console.log("******** Server berjalan ********")
    console.log(JSON.stringify({start: dateFormat(dateRun), end: getOneHourLater(dateRun)}, null, 4))
    console.log("******** ******* ******* ********")
});

// setTimeout(() => {
//     console.log("******** Server berhenti " + new Date() + " ********")
//     process.exit(0);
// }, TIME_LIMIT_RUNNING) // 1 hours